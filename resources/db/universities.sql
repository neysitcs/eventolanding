INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10164, 604, 'Escuela Nacional de Marina Mercante Almirante Miguel Grau', 'http://www.enamm.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10165, 604, 'Facultad de Medicina Hipolito Unanue Universidad Nacional Federico Villarreal', 'http://medicinaunfv.org/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10166, 604, 'Instituto Peruano de Administración de Empresas', 'http://www.ipae.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10167, 604, 'Instituto Superior Tecnológico', 'http://www.idat.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10168, 604, 'Instituto Superior Tecnologio Cibertec', 'http://www.cibertec.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10169, 604, 'Instituto Técnico de Administración de Empresas', 'http://www.itae.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10170, 604, 'Pontificia Universidad Católica del Perú', 'http://www.pucp.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10171, 604, 'TECSUP', 'http://www.tecsup.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10172, 604, 'Universidad Alas Peruanas', 'http://www.uap.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10173, 604, 'Universidad Andina del Cusco', 'http://www.uandina.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10174, 604, 'Universidad Andina Nestor Caceres Velasquez', 'http://www.uancv.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10175, 604, 'Universidad Antonio Ruiz de Montoya', 'http://www.uarm.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10176, 604, 'Universidad Católica de Santa María', 'http://www.ucsm.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10177, 604, 'Universidad Católica de Trujillo', 'http://www.uct.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10178, 604, 'Universidad Católica San Pablo Arequipa', 'http://www.usp.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10179, 604, 'Universidad Católica Santo Toribio de Mogrovejo', 'http://www.usat.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10180, 604, 'Universidad Católica Sedes Sapientiae', 'http://www.ucss.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10181, 604, 'Universidad Cesar Vallejo *', 'http://www.ucv.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10182, 604, 'Universidad Científica del Sur', 'http://www.ucsur.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10183, 604, 'Universidad Continental de Ciencias e Ingenieria', 'http://www.continental.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10184, 604, 'Universidad de Chiclayo', 'http://www.udch.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10185, 604, 'Universidad de Huánuco', 'http://www.udh.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10186, 604, 'Universidad de Lima', 'http://www.ulima.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10187, 604, 'Universidad de Piura', 'http://www.udep.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10188, 604, 'Universidad de San Martin de Porres', 'http://www.usmp.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10189, 604, 'Universidad del Pacífico Perú', 'http://www.up.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10190, 604, 'Universidad ESAN', 'http://www.esan.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10191, 604, 'Universidad Femenina del Sagrado Corazon', 'http://www.unife.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10192, 604, 'Universidad Inca Garcilaso de la Vega', 'http://www.uigv.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10193, 604, 'Universidad José Carlos Mariategui', 'http://www.ujcm.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10194, 604, 'Universidad Los Ángeles de Chimbote', 'http://www.uladech.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10195, 604, 'Universidad Marcelino Champagnat', 'http://www.umch.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10196, 604, 'Universidad Nacional Agraria de la Selva Tingo María', 'http://www.unas.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10197, 604, 'Universidad Nacional Agraria La Molina', 'http://www.lamolina.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10198, 604, 'Universidad Nacional Amazónica de Madre de Dios', 'http://www.unamad.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10199, 604, 'Universidad Nacional Daniel Alcides Carrion', 'http://www.undac.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10200, 604, 'Universidad Nacional de Cajamarca', 'http://www.unc.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10201, 604, 'Universidad Nacional de Educación', 'http://www.une.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10202, 604, 'Universidad Nacional de Huancavelica', 'http://www.unh.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10203, 604, 'Universidad Nacional de Ingeniería Lima', 'http://www.uni.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10204, 604, 'Universidad Nacional de la Amazonía Peruana', 'http://www.unapiquitos.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10205, 604, 'Universidad Nacional de Piura', 'http://www.unp.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10206, 604, 'Universidad Nacional de San Agustín de Arequipa', 'http://www.unsa.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10207, 604, 'Universidad Nacional de San Antonio Abad del Cusco', 'http://www.unsaac.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10208, 604, 'Universidad Nacional de San Cristobal de Huamanga', 'http://www.unsch.galeon.com/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10209, 604, 'Universidad Nacional de San Martín Tarapoto', 'http://www.unsm.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10210, 604, 'Universidad Nacional de Trujillo', 'http://www.unitru.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10211, 604, 'Universidad Nacional de Tumbes', 'http://www.untumbes.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10212, 604, 'Universidad Nacional de Ucayali', 'http://www.unu.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10213, 604, 'Universidad Nacional del Altiplano', 'http://www.unap.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10214, 604, 'Universidad Nacional del Callao', 'http://www.unac.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10215, 604, 'Universidad Nacional del Centro del Perú', 'http://www.uncp.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10216, 604, 'Universidad Nacional del Santa Chimbote', 'http://www.uns.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10217, 604, 'Universidad Nacional Federico Villarreal', 'http://www.unfv.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10218, 604, 'Universidad Nacional Hermilio Valdizán', 'http://www.unheval.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10219, 604, 'Universidad Nacional Jorge Basadre Grohmann', 'http://www.unjbg.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10220, 604, 'Universidad Nacional José Faustino Sanchez Carrión', 'http://www.unjfsc.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10221, 604, 'Universidad Nacional Mayor de San Marcos', 'http://www.unmsm.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10222, 604, 'Universidad Nacional Micaela Bastidas de Apurímac', 'http://www.unamba.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10223, 604, 'Universidad Nacional Pedro Ruiz Gallo', 'http://www.unprg.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10224, 604, 'Universidad Nacional San Cristobal de Huamanga', 'http://www.unsch.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10225, 604, 'Universidad Nacional San Luis Gonzaga de Ica', 'http://www.unica.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10226, 604, 'Universidad Nacional Santiago Antunez de Mayolo', 'http://www.unasam.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10227, 604, 'Universidad Nacional Toribio Rodriguez de Mendoza de Amazonas', 'http://www.unatamazonas.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10228, 604, 'Universidad Norbert Wiener', 'http://www.uwiener.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10229, 604, 'Universidad para el Desarrollo Andino', 'http://www.udea.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10230, 604, 'Universidad Particular Tecnológica de los Andes', 'http://www.utea.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10231, 604, 'Universidad Peruana Cayetano Heredia', 'http://www.upch.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10232, 604, 'Universidad Peruana de Ciencias Aplicadas', 'http://www.upc.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10233, 604, 'Universidad Peruana de Ciencias e Informática', 'http://www.upci.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10234, 604, 'Universidad Peruana de las Américas', 'http://www.ulasamericas.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10235, 604, 'Universidad Peruana los Andes', 'http://www.upla.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10236, 604, 'Universidad Privada Ada A Byron', 'http://upab.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10237, 604, 'Universidad Privada Antenor Orrego', 'http://www.upao.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10238, 604, 'Universidad Privada Antonio Guillermo Urrelo', 'http://www.upagu.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10239, 604, 'Universidad Privada de Pucallpa', 'http://www.uppucallpa.org/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10240, 604, 'Universidad Privada de Tacna', 'http://www.upt.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10241, 604, 'Universidad Privada de Trujillo', 'http://www.uptrujillo.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10242, 604, 'Universidad Privada del Norte', 'http://www.upnorte.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10243, 604, 'Universidad Privada San Juan Bautista', 'http://www.upsjb.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10244, 604, 'Universidad Privada San Pedro', 'http://www.upsp.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10245, 604, 'Universidad Privada Sergio Bernales', 'http://www.upsb.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10246, 604, 'Universidad Privada Telesup', 'http://www.telesup.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10247, 604, 'Universidad Ricardo Palma', 'http://www.urp.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10248, 604, 'Universidad San Ignacio de Loyola', 'http://www.usil.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10249, 604, 'Universidad Señor de Sipan', 'http://www.uss.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10250, 604, 'Universidad Tecnológica del Cono Sur', 'http://www.untecs.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10251, 604, 'Universidad Tecnológica del Perú', 'http://www.utp.edu.pe/', null, null);
INSERT INTO universities (id, country_id, name, url, logo, icon) VALUES (10252, 604, 'Universidad Peruana Unión    ', 'http://www.upeu.edu.pe/', null, null);
