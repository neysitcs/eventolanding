<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditRequest extends FormRequest
{

    protected $redirect = '/#edit';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = auth()->id();
        return [
            'name'=>'required|min:3',
            'last_name'=>'required',
            'email_register'=>'required|email|unique:users,email,'.$id,
            'password_register'=>'nullable|min:5',
            'dni'=>'nullable|size:8',
            'city'=>'',
            'prefix'=>'nullable|numeric',
            'phone'=>'nullable|numeric',
            'university'=>'',
            'facultad'=>'',
            'grado_academico'=>'',
            'category'=>'required',
        ];
    }
}
