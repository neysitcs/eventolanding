@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Usuarios</div>

                <div class="panel-body">

                  <div class="table-responsive">
                    <table class="table table-striped table-hover ">

                      <thead>
                          <tr>
                            <th>Nombre</th>
                            <th>Apellidos</th>
                            <th>Dni</th>
                            <th>Ciudad</th>
                            <th>Teléfono</th>
                            <th>Universidad</th>
                            <th>Facultad</th>
                            <th>Grado</th>
                            <th>Email</th>
                            <th>Archivo</th>
                          </tr>
                      </thead>

                      <tbody>

                        @foreach ($users as $key => $value)
                          <tr>
                            <td>{{$value->name}}</td>
                            <td>{{$value->last_name}}</td>
                            <td>{{$value->dni}}</td>
                            <td>{{$value->city}}</td>
                            <td>{{$value->prefix . ' ' . $value->phone}}</td>
                            <td>{{$value->university}}</td>
                            <td>{{$value->facultad}}</td>
                            <td>{{$value->grado_academico}}</td>
                            <td>{{$value->email}}</td>
                            <td>
                              @if (!empty($value->link_file))
                                    <a href="{{asset($value->link_file)}}"><b>Archivo &darr;</b></a>
                            @else
                                A. Pendiente  
                              @endif


                            </td>
                          </tr>

                        @endforeach

                      </tbody>

                    </table>
                  </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
