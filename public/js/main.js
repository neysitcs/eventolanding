var loginOpened = false;
$(document).ready(function () {

  $('.btn-login').on('click',function () {
    $("html, body").stop(true,true).animate({scrollTop: $('#loginx').offset().top }, 1000);
      $('.section-login').slideToggle();
  });

  $('.show-more').on('click',function () {

        $(this).fadeOut(1,function () {
            $('.more').fadeIn();
        });
  });

  $('.link-edit').on('click',function () {

        $('.bar-edit,.form-edit').fadeIn();
        $('.form-upload ').fadeOut();
  });
  $('.bar-edit span').on('click',function () {

      $('.bar-edit,.form-edit').fadeOut(function () {
            $('.form-upload ').fadeIn();
      });

  });




  $('.infot').on('click', function (event) {

      var sel = $(this).data('to');
      $('#'+sel).toggle();
  });

  $(document).on('click', function (event) {
    if($(event.target).hasClass('infot')==false){
        $('.toolx').fadeOut('fast');
    }

  });


  $(document).on('click', function (event) {
    if($(event.target).hasClass('navbar-toggle')==false){
        $(".navbar-collapse").collapse('hide');
    }

  });


  $(document).on('touchmove', function (event) {
    if($(event.target).hasClass('navbar-toggle')==false){
        $(".navbar-collapse").collapse('hide');
      }
  });


});


$(document).on('click', 'a[href^="#"]', function (event) {
    // event.preventDefault();

    if(!$(event.target).hasClass('btn-login')){
          $('.section-login').hide(0);
    }

    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      $('html, body').animate({
          scrollTop: $($.attr(this, 'href')).offset().top - 390
      }, 500);
   }else {

     $('html, body').animate({
         scrollTop: $($.attr(this, 'href')).offset().top - 30
     }, 500);

   }


});


$(function(){
     var navMain = $(".navbar-collapse"); // avoid dependency on #id
     // "a:not([data-toggle])" - to avoid issues caused
     // when you have dropdown inside navbar
     navMain.on("click", "a:not([data-toggle])", null, function () {
         navMain.collapse('hide');
     });
 });


$(function() {

  // We can attach the `fileselect` event to all file inputs on the page
  $(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });

  // We can watch for our custom `fileselect` event like this
  $(document).ready( function() {
      $(':file').on('fileselect', function(event, numFiles, label) {

          var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;

          if( input.length ) {
              input.val(log);
          } else {
              if( log ) alert(log);
          }

      });
  });

});
