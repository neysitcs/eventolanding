<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('last_name')->nullable();
            $table->string('dni')->nullable();
            $table->string('city')->nullable();
            $table->string('prefix')->nullable();
            $table->string('phone')->nullable();
            $table->string('university')->nullable();
            $table->string('facultad')->nullable();
            $table->string('grado_academico')->nullable();
            $table->string('category_name')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('role')->default('user');
            $table->boolean('complete')->default(false);
            $table->boolean('cancelado')->default(false);
            $table->text('link_file')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
