function uploadImage(image) {
    var data = new FormData();
    data.append("image", image);
    $.ajax({
        headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/upload',
        cache: false,
        contentType: false,
        processData: false,
        data: data,
        type: "post",
        success: function(url) {
            var image = $('<img>').attr('src', url);
            $('#image').val(url);
            $('#summernote').summernote("insertNode", image[0]);
        },
        error: function(data) {
            console.log(data);
        }
    });
}


$(document).ready(function() {
    $('#summernote').summernote({
      height: 200,

      callbacks: {
          onImageUpload: function(image) {
              uploadImage(image[0]);
          }
      }

    });
});
