<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout')->name('logoutx');
Route::post('/registeruser', 'HomeController@registerUser')->name('register.user');
Route::post('/edituser', 'HomeController@editUser')->name('edit.user');

Route::get('/cancel', 'HomeController@cancel')->name('cancel');

Route::post('/password', 'HomeController@resetPassword')->name('password');


Route::post('/makeContact', 'HomeController@makeContact')->name('contact');

Route::get('/admin', 'AdminController@index')->name('admin');
Route::get('/users', 'AdminController@users')->name('admin.users');

Route::post('/news/create', 'AdminController@newsCreate')->name('news.create');
Route::get('/news', 'AdminController@newsIndex')->name('news.index');
Route::get('/news/delete/{id}', 'AdminController@deletenew')->name('news.delete');
Route::get('/news/edit/{id}', 'AdminController@edit')->name('news.edit');
Route::post('/news/update/{id}', 'AdminController@update')->name('news.update');

Route::post('/upload', 'AdminController@uploadFile')->name('news.upload');


Route::get('/noticias', 'HomeController@showNotice')->name('news.show');
Route::get('/privacy', 'HomeController@privacy')->name('privacy');
Route::get('/terms', 'HomeController@terms')->name('terms');


Route::post('/sendproject', 'HomeController@sendProject')->name('user.project');
