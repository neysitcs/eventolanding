<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

  <link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet">


  <!-- include summernote css/js -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
</head>
<body>


  <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <nav id="spy">
                <ul class="sidebar-nav nav">
                    <li class="sidebar-brand">
                        <a href="/"><span class="fa fa-home solo">ADMIN</span></a>
                    </li>
                    <li>
                        <a href="/admin" data-scroll>
                            <span class="fa fa-anchor solo">Crear Noticias</span>
                        </a>
                    </li>

                    <li>
                        <a href="/news" data-scroll>
                            <span class="fa fa-anchor solo">Noticias</span>
                        </a>
                    </li>

                    <li>
                        <a href="/users" data-scroll>
                            <span class="fa fa-anchor solo">Participantes</span>
                        </a>
                    </li>

                </ul>
            </nav>
        </div>

        <!-- Page content -->
        <div id="page-content-wrapper">


              @yield('content')

        </div>

    </div>


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

    <script src="{{ asset('js/admin.js') }}"></script>
</body>
</html>
