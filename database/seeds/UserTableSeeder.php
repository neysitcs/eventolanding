<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = new App\User;
        $user->name = 'Admin';
        $user->email = 'mrios@rhadministraciones.com.pe';
        $user->password = bcrypt('sindrogas');
        $user->role = 'admin';
        $user->save();
    }
}
