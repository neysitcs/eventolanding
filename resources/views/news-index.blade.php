@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Noticias</div>

                <div class="panel-body">

                    <table class="table table-striped table-hover ">

                      <thead>
                          <tr>
                            <td>Título</td>
                            <td>Descripción</td>
                            <td>Fecha</td>
                            <td>Foto</td>
                          </tr>
                      </thead>

                      <tbody>

                        @foreach ($posts as $key => $value)
                          <tr>
                            <td>{{$value->title}}</td>
                            <td>{{ substr(strip_tags($value->post), 0 , 100)}}</td>
                            <td>{{$value->date}}</td>
                            <td><img width="40" height="auto" src={{asset($value->image)}}/></td>
                            <td><a href="{{route('news.edit', $value->id)}}">Editar</a></td>
                            <td><a href="{{route('news.delete', $value->id)}}">Eliminar</a></td>
                          </tr>

                        @endforeach

                      </tbody>

                    </table>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
