<tr>
    <td class="footer-wrapper">
        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0">
            <tr>
                <td class="content-cell" align="center">
                    <span class="mini">Si en algún momento deseas dejar de recibir nuestros mensajes, puedes hacerlo aquí:</span><br/>
                    {{ Illuminate\Mail\Markdown::parse($slot) }}
                </td>
            </tr>
        </table>
    </td>
</tr>
