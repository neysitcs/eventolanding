@extends('layouts.app')

@section('content')


<div id="loginx" class="section section-login group" @if($errors->has('email') or  $errors->has('password') or $errors->has('password'))  style="display:block;" @endif>

  <div class="col-sm-6 col-md-4 col-md-offset-4">

          <div class="panel-body">
              <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                  {{ csrf_field() }}

                  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                      <label for="email" class="col-md-3 control-label">Correo</label>

                      <div class="col-md-9">
                          <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                          @if ($errors->has('email'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>

                  <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                      <label for="password" class="col-md-3 control-label">Contraseña
                      </label>

                      <div class="col-md-9">
                          <input id="password" type="password" class="form-control" name="password" required>

                          @if ($errors->has('password'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('password') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>



                  <div class="form-group">
                      <div class="col-md-6 col-xs-offset-4 col-sm-offset-5 col-md-offset-6">

                          <button type="submit" class="btn btn-danger">
                              Iniciar
                          </button>

                      </div>
                  </div>
              </form>


              <form class="form-horizontal" method="POST" action="{{ url('password') }}">
                  {{ csrf_field() }}

                <p class="col-md-10  col-md-offset-3 forgot">¿Recordar contraseña?</p>

              <div class="form-group{{ $errors->has('emailreset') ? ' has-error' : '' }}">
                  <label for="emailreset" class="col-md-3 control-label">Correo</label>

                  <div class="col-md-9">
                      <input id="emailreset" type="email" class="form-control" name="emailreset" value="{{ old('emailreset') }}" required autofocus>
                      <span class="info">* Enviaremos tu contraseña al correo indicado</span>

                      @if ($errors->has('emailreset'))
                          <span class="help-block">
                              <strong>{{ $errors->first('emailreset') }}</strong>
                          </span>
                      @endif

                      <button type="submit" class="btn btn-default btn-sent">
                          ENVIAR
                      </button>
                  </div>
              </div>

            </form>



  </div>
  </div>

</div>

<div class="section section-head group" id="homex">
<div class="container">
    <div class="row">

       <img class="person-left visible-md visible-lg" src="{{asset('images/left.png')}}">
        <div class="col-md-8 col-md-offset-2 col-lg-10 col-lg-offset-1">
          <div class="head-text">

            <div class="title-y">
              — CONCURSO —
            </div>

            <div class="title-r">
             CULTURA POR<br> UNFUTURO<br>  SIN DROGAS
            </div>
            <br/>
            <div class="title-w">
             ELABORA UN PROYECTO SOBRE:
            </div>

            <div class="title-y title-border">
              EDUCACIÓN AL NO CONSUMO Ó PROMOCIÓN DE <br/> CULTIVOS ALTERNATIVOS DE LA AMAZONÍA.
            </div>

            <div class="title-w">
               LOS MEJORES PROYECTOS SERÁN PREMIADOS <br>
               1. $3500  / 2. $1500 / 3. $1000

               <br/>
             </div>
           </div>

             <img class="logof logotop" src="{{asset('/images/logo-fundacion.png')}}"/>

        </div>

         <img class="person-right visible-md visible-lg" src="{{asset('images/right.png')}}">
    </div>
</div>
</div>

@if(auth()->check())
    @include('upload')
@else
  @include('register')
@endif



<div class="section section-bases group" id="bases">

    <div class="container">
        <h2 class="title">Bases del concurso</h2>

        <p class="center">
          La Fundación Cinco Océanos Profundos convoca a la primera edición del premio: CULTURA POR UN FUTURO SIN DROGAS, para la creación y difusión de valores entre los jóvenes. Al que pueden concurrir estudiantes de cualquier universidad del país mediante la presentación de un trabajo, el cual debe contener una propuesta que ha de basarse en los siguientes temas: EDUCACIÓN PARA EL NO CONSUMO o PROMOCIÓN DE CULTIVOS ALTERNATIVOS DE LA AMAZONÍA. Estos trabajos deben ser de carácter inédito y de autoría individual o colectiva.
        </p>


<div class="collapse-base">

<a data-toggle="collapse" class="collapsed" data-target="#base1" href="javascript:;">PRIMERA</a>
<div id="base1" class="collapse">
Se debe participar mediante la presentación de un artículo sobre cualquiera de los temas, de carácter inédito y de autoría individual o colectiva. Los trabajos han de ser originales, escritos en castellano y podrán proceder de alumnos de cualquier universidad del país. Estos trabajos no pueden haber sido aceptados ni entregados para su publicación en revista o editorial alguna.
</div>

<a data-toggle="collapse"  class="collapsed"  data-target="#base2" href="javascript:;">SEGUNDA</a>
<div id="base2" class="collapse">
Los artículos deben tratar sobre la cultura de prevención de consumo de drogas y es posible tratar sobre un tema ya publicado siempre y cuando sea con otra visión.
</div>


<a data-toggle="collapse"  class="collapsed"  data-target="#base3" href="javascript:;">TERCERA</a>
<div id="base3" class="collapse">

  Los trabajos se presentarán escritos en <i>Word</i>, empleando la fuente <i>Arial</i> con un tamaño de 11 y con espacio y medio de interlineado, los mismos que deberán tener una extensión mínima de 20 hojas y una máxima de 40 hojas. El desarrollo debe seguir la estructura propuesta en el concurso: Presentación, Marco Histórico, Marco Legal, Marco Teórico, Desarrollo y Propuesta. Se entregarán tres ejemplares bajo un seudónimo y acompañados con un sobre cerrado donde se consignaran el o los nombres de los autores del trabajo, teléfonos, dirección, universidad, carrera, copia del carné universitario y un CD con el trabajo en <i>Word</i>.
  <br/>
Los trabajos serán remitidos al local de la fundación, ubicada en calle Los Tulipanes N.º 147, oficina 405, urbanización El Polo - Santiago de Surco en Lima, y en el exterior del sobre deberá consignarse el título del trabajo presentado y el seudónimo.


</div>


<a data-toggle="collapse"  class="collapsed"  data-target="#base4" href="javascript:;">CUARTA</a>
<div id="base4" class="collapse">
  El plazo de recepción de los trabajos finalizará a las 12:00 horas del día 20 de febrero de 2018.
</div>

<a data-toggle="collapse"  class="collapsed"  data-target="#base5" href="javascript:;">QUINTA</a>
<div id="base5" class="collapse">
La junta directiva de la fundación procederá a la elección de los miembros del jurado que calificarán los trabajos y decidirán a los ganadores del concurso, los cuales serán personajes de reconocido prestigio en los temas que se han de tratar y una persona escogida por la fundación quien será el secretario del jurado.

</div>


<a data-toggle="collapse"  class="collapsed"  data-target="#base6" href="javascript:;">SEXTA</a>
<div id="base6" class="collapse">
El fallo del jurado se producirá dentro de los cuarenta y cinco días calendarios posteriores a la finalización del plazo de recepción de los trabajos, luego de lo cual se harán públicos los resultados por medio de la página web de la fundación, del concurso  y por las redes sociales.

</div>

<a data-toggle="collapse"  class="collapsed"  data-target="#base7" href="javascript:;">SÉPTIMA</a>
<div id="base7" class="collapse">
  Los premios son en dólares americanos y se dividen de la siguiente manera: TRES MIL QUINIENTOS DÓLARES para el primer lugar, MIL QUINIENTOS DÓLARES para el segundo lugar y MIL DÓLARES para el tercer lugar por cada tema. Además habrá tres diplomas con mención honrosa por cada tema. <br/>
  La fundación asumirá cualquier impuesto que pueda generarse, por lo que el ganador recibirá el monto neto del premio. Así mismo, la fundación cubrirá todos los gastos de transporte, hospedaje y alimentación para los ganadores que vivan fuera de ciudad de Lima y puedan asistir a la ceremonia de premiación.


</div>


<a data-toggle="collapse"  class="collapsed"  data-target="#base8" href="javascript:;">OCTAVA</a>
<div id="base8" class="collapse">
  El jurado se reserva la facultad de declarar desierto el premio o de otorgarlo a más de un trabajo, para lo que dividiría el monto en partes iguales en este último caso.


</div>
<a data-toggle="collapse"  class="collapsed"  data-target="#base9" href="javascript:;">NOVENA</a>
<div id="base9" class="collapse">
Los trabajos premiados serán incluidos en un libro que será editado y publicado por la fundación, el cual luego será enviado al poder ejecutivo como una propuesta para el mejoramiento de la cultura de prevención en el consumo de drogas y cultivos alternativos.

</div>


<a data-toggle="collapse"  class="collapsed"  data-target="#base10" href="javascript:;">DÉCIMA</a>
<div id="base10" class="collapse">
La entrega de los premios será en un acto público, el cual será previamente anunciado por los canales respectivos.


</div>

<a data-toggle="collapse"  class="collapsed"  data-target="#base11" href="javascript:;">UNDÉCIMA</a>
<div id="base11" class="collapse">
  La participación en el concurso supone la aceptación total de las bases anteriormente expuestas.

</div>
<a data-toggle="collapse"  class="collapsed"  data-target="#base12" href="javascript:;">DUODÉCIMA</a>
<div id="base12" class="collapse">
  Para la facilidad del envío de los trabajos, estos podrán ser enviados también a través de la página web del concurso: <a href="http://www.futurosindrogas.com">www.futurosindrogas.com</a><br/>

  El desarrollo de la propuesta deberá ser igual a lo indicado en la tercera regla de las bases del concurso.<br/>

Solo se deberá enviar una copia del trabajo acompañada por de los nombres de los autores del trabajo, teléfonos, dirección, universidad, carrera y una imagen del carnet universitario.


</div>



</div>

        <br id="faq"/>

          <h2 class="title">Preguntas Frecuentes</h2>

          <div class="faq col-md-8 col-md-offset-2">

          <h3>¿Qué debo hacer para participar en el concurso?</h3>
          <p>
            Para participar en el concurso, en primer lugar, se tendrá que realizar una inscripción a través de los formularios publicados en el sitio web de la Fundación Cinco Océanos Profundo y los Fan Page de la fundación y del concurso.</p>
          <p>Posteriormente, solo queda realizar el envío de los trabajos, según lo establecido en las bases del concurso, las mismas que pueden obtener a través de los mismos sitio web y fan page indicados.</p>

          <h3>¿Cómo puedo inscribir al concurso y dónde puedo encontrar las bases del concurso?</h3>
          <p>
          La  inscripción se realizará a través de los siguientes enlaces de internet y allí mismo están colgadas las bases del concurso:  <br/>
          <a href="http://www.futurosindrogas.com">www.futurosindrogas.com</a><br/>
          <a href="https://www.facebook.com/cincooceanosprofundoperu/app/684336108254110/">https://www.facebook.com/cincooceanosprofundoperu/app/684336108254110/</a><br/>
          <a href="https://www.facebook.com/Concurso-Cultura-por-un-Futuro-sin-Drogas-Edición-2018-483825981984603/">https://www.facebook.com/Concurso-Cultura-por-un-Futuro-sin-Drogas-Edición-2018-483825981984603/</a>

        </p>




  	<h3>¿Cuántos integrantes puede tener si es un grupo el que participa?</h3>

Los trabajos pueden ser presentados de manera individual o en grupos de hasta 4 integrantes.

  	<h3>¿Cómo puedo obtener el libro o bajarlo por PDF?</h3>

    El libro puede ser adquirido en la cadena de librerías Ibero, o también puede ser descargado en versión PDF del sitio web de la Fundación y de los fan page de la Fundación y el concurso, en Facebook.
      <br/>  <br/>
      <a class="center show-more" href="javascript:;"><img  src="{{asset('images/plus.png')}}"/></a>
  <div class="more">

  	<h3>¿El concurso es solo para estudiantes de pre grado?</h3>

  Sí. El concurso solo está abierto a alumnos de pregrado, que aún estén cursando sus estudios universitarios, es decir, que aún no hayan logrado el grado académico de bachiller o licenciado. Los grupos pueden estar conformados por estudiantes de diferentes universidades.

  	<h3>¿El trabajo debe estar basado en el libro?</h3>

  El libro Antidrogas, Nueva Visión para enfrentar con Éxito el Tráfico Ilegal, de Javier Reátegui Rosselló es la principal referencia para la realización del trabajo, ya que en él están desarrollados los principios y conceptos de las dos líneas temáticas del concurso.

  	<h3>¿Pueden participar estudiantes de institutos?</h3>

    No. En la edición 2018 no podrán participar estudiantes de institutos de educación superior, sólo alumnos universitarios.


  	<h3>¿Qué temas deben tratar los trabajos?</h3>

    Los dos temas que deberán ser tratados son: Educación al No Consumo de drogas y Promoción de Cultivos Alternativos de la Amazonía. El trabajo que se presente deberá tratar sobre uno de los dos temas.<br/>
    Ejemplo:<br/>
    Tema: Promoción de cultivos alternativos amazónicos <br/>
    Trabajo: Producción de Aguaje en la Región Loreto


  	<h3>¿Cómo se hará la entrega de los trabajos tanto en Lima como en provincias?</h3>

    Para la facilidad del envío de los trabajos, estos podrán ser enviados también a través de la página web del concurso:
    <a href="http://www.futurosindrogas.com">www.futurosindrogas.com</a> <br/>
    El desarrollo de la propuesta deberá ser igual a lo indicado en la tercera regla de las bases del concurso. Solo se deberá enviar una copia del trabajo acompañada por los nombres de los autores del trabajo, teléfonos, dirección, universidad, carrera y una imagen del carnet universitario.<br/>
    De optar por la entrega física esta será en la dirección  Calle Los Tulipanes 147, Of. 405, Urb. El Polo, Santiago de Surco, en el horario de 9 am a 6 pm. <br/>
    El plazo para cualquiera de los dos envíos  vence el 20 de febrero de 2018 a las 12 del mediodía.<br/>


  	<h3>Sobre el Marco legal</h3>

El marco legal está conformado por todas aquellas normativas que hayan sido emitidas en el tiempo por los organismos oficiales relacionados con estos temas. Será considerado solo de manera referencial para contextualizar el desarrollo de la propuesta.

  	<h3>Sobre el Marco histórico</h3>

El marco histórico hace referencia a todo el contexto en el que se ha tratado el tema de las drogas, es decir, el inicio del problema, enfoques principales, principales acontecimientos, quienes fueron los principales actores en este tema. Así mismo según sea el tema, sobre todo lo relacionado en el tiempo con el desarrollo  de educación sobre drogas  y con la promoción de cultivos, también considerando para contextualizar  el desarrollo de la propuesta.

  	<h3>Sobre el Marco teórico</h3>
El marco teórico, hará referencia  a los conceptos, teorías, trabajos e investigaciones sobre la coca  y sus derivados, así como la educación  sobre las drogas y la promoción  de cultivos, según sea el tema escogido, siempre considerando para contextualizar el desarrollo de la propuesta.

</div>

</div>

    </div>
</div>






<div class="section section-noticias group" id="noticias">
  <br/>
  <h2 class="title">Noticias</h2>

  <div id="carromobile" class="carousel slide visible-xs" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#carromobile" data-slide-to="0" class="active"></li>
      <li data-target="#carromobile" data-slide-to="1"></li>
      <li data-target="#carromobile" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
 <div class="carousel-inner">
     @foreach ($newsmobile as $key => $value)
   <div class="item @if($key==0) active @endif">
      <div class="col-sm-4">
        <div class="new">
            <img src="{{asset($value['image'])}}">
            <div class="new_title">{{$value['title']}}</div>
            <div class="new-wrapper">
            <div class="new_content">{{ substr(strip_tags($value['post']),0,100) }}</div>
          <div class="new_date">{{ (new DateTime($value['date']))->format('d/m/Y') }}</div>
            <br>
             <a class="readmore" href="/noticias?pid={{$value['id']}}">LEER MÁS</a>
              <br><br>
        </div>
        </div>
        </div>
   </div>

     @endforeach

  </div>
 </div>

  <div id="myCarousel" class="carousel slide hidden-xs" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
 <div class="carousel-inner">
   <div class="item active">

    @foreach ($news[0] as $key => $value)
      <div class="@if($key > 0) hidden-xs @endif col-sm-4">
        <div class="new">
            <img src="{{asset($value['image'])}}">
            <div class="new_title">{{$value['title']}}</div>
            <div class="new-wrapper">
            <div class="new_content">{{ substr(strip_tags($value['post']),0,100) }}</div>
          <div class="new_date">{{ (new DateTime($value['date']))->format('d/m/Y') }}</div>
            <br>
             <a class="readmore" href="/noticias?pid={{$value['id']}}">LEER MÁS</a>
              <br><br>
        </div>
        </div>
        </div>
    @endforeach


   </div>

   <div class="item">

     @if (isset($news[1]))

     @foreach ($news[1] as $key => $value)
       <div class="@if($key > 0) hidden-xs @endif col-sm-4">
         <div class="new">
             <img src="{{asset($value['image'])}}">
             <div class="new_title">{{$value['title']}}</div>
               <div class="new-wrapper">
             <div class="new_content">{{ substr(strip_tags($value['post']),0,100) }}</div>
            <div class="new_date">{{ (new DateTime($value['date']))->format('d/m/Y') }}</div>
            <br>
             <a class="readmore" href="/noticias?pid={{$value['id']}}">LEER MÁS</a>
              <br><br>
         </div>
         </div>
         </div>
     @endforeach

     @endif

   </div>

   <div class="item">
    @if (isset($news[2]))
     @foreach ($news[2] as $key => $value)
       <div class="@if($key > 0) hidden-xs @endif col-sm-4">
         <div class="new">
             <img src="{{asset($value['image'])}}">
             <div class="new_title">{{$value['title']}}</div>
               <div class="new-wrapper">
             <div class="new_content">{{ substr(strip_tags($value['post']),0,100) }}</div>
             <div class="new_date">{{ (new DateTime($value['date']))->format('d/m/Y') }}</div>

             <br>
              <a class="readmore" href="/noticias?pid={{$value['id']}}">LEER MÁS</a>
               <br><br>

         </div>
         </div>
         </div>
     @endforeach
   @endif
   </div>
 </div>



</div>

<hr/>

<div class="section section-contacto group" id="contacto">

  <div class="container">
        <h2 class="title">Contacto</h2>

        @if (session('success'))
            <div class="alert alert-success">{{session('success')}}</div>
        @endif

            <div class="form-contact col-md-8 col-md-offset-2">
        {!! Form::open(['method'=>'post','url' => route('contact'),'class'=>'form-horizontal']) !!}

          <div class="form-group{{ $errors->has('name_contact') ? ' has-error' : '' }}">
           {{ Form::label('*Nombres', null, ['class' => 'col-sm-3 control-label']) }}
            <div class="col-sm-7">
                {!!  Form::text('name_contact', '', ['class'=>'form-control']) !!}
                @if ($errors->has('name_contact'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name_contact') }}</strong>
                    </span>
                @endif
            </div>
          </div>


          <div class="form-group{{ $errors->has('email_contact') ? ' has-error' : '' }}">
           {{ Form::label('*Email', null, ['class' => 'col-sm-3 control-label']) }}
            <div class="col-sm-7">
                {!!  Form::email('email_contact', '', ['class'=>'form-control']) !!}
                @if ($errors->has('email_contact'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email_contact') }}</strong>
                    </span>
                @endif
            </div>
          </div>

          <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
           {{ Form::label('*Mensaje', null, ['class' => 'col-sm-3 control-label']) }}
            <div class="col-sm-9">
                {!!  Form::textarea('message', '', ['class'=>'form-control']) !!}
                @if ($errors->has('message'))
                    <span class="help-block">
                        <strong>{{ $errors->first('message') }}</strong>
                    </span>
                @endif
            </div>
          </div>
          <div class="form-group">
              <div class="col-sm-3"></div>
           {{ Form::label('(*) Campos obligatorios', '', ['class' => 'col-sm-3 control-label']) }}
          </div>

          <div class="form-group">
                  <div class="hidden-xs col-sm-9"></div>
                  <div class="col-xs-6 col-sm-3">
                      {!!  Form::submit('Enviar', ['class'=>'btn btn-danger btn-block pull-right']) !!}
                  </div>


          </div>


          </div>


{!! Form::close() !!}

  </div>

</div>

@endsection
