<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\UserRequest;
use App\Http\Requests\EditRequest;
use App\Http\Requests\PassRequest;
use App\Http\Requests\ContactRequest;

use App\University;
use App\User;
use App\Post;
use App\Notifications\SentPassword;
use App\Notifications\NewUserRegistration;
use App\Notifications\ContactNotificaction;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $regions = config('global.regions');
      $degrees = config('global.degrees');
      $universities = University::all()->pluck('name','name')->prepend('Seleccione');

      $regions = array_combine($regions,$regions);


      $user = @auth()->user();
      $news = Post::orderBy('created_at','DESC')->get()->toArray();
        $newsmobile = $news;
      $news = array_chunk($news,3);

      return view('welcome',compact('regions','degrees','universities','user','news','newsmobile'));

    }

    public function registerUser(UserRequest $request)
    {
      if(auth()->check())
            return redirect('/#upload');

      $data = $request->all();
      $data['email'] = $request->email_register;
      $data['password'] =bcrypt($request->password_register);
      $data['category_name'] =($request->category);

      $user = new User();
      $user->fill($data);
      $user->save();

      auth()->login($user);

      $user->notify( new NewUserRegistration($user, $request->password_register));

        return redirect('/#upload');
    }

    public function editUser(EditRequest $request)
    {

      $data = $request->all();
      $data['email'] = $request->email_register;
      if(!empty($request->password_register))
          $data['password'] =bcrypt($request->password_register);
      $data['category_name'] =($request->category);

      $user = auth()->user();
      $user->fill($data);
      $user->save();

      // auth()->login($user);

      return redirect('/#upload');
    }


    public function resetPassword(PassRequest $request)
    {

      $email = $request->emailreset;

      $user = User::where('email',$email)->first();

      $pass =  sprintf("%06d", mt_rand(1, 999999));


      $user->notify(new SentPassword($user, $pass));

      $user->password = bcrypt($pass);
      $user->save();

      return redirect()->back();


    }


    public function makeContact(ContactRequest $request)
    {

      (new User)->forceFill([
          'name' => 'Secretaría',
          'email' => config('global.contact'),
      ])->notify( new ContactNotificaction($request->name_contact, $request->message, $request->email_contact));

        return redirect()->to('/#contacto')->with('success','Tu mensaje ha sido enviado correctamente');
    }


    public function showNotice(Request $request)
    {
      $post = Post::findOrFail($request->pid);
      $news = Post::whereNotIn('id',[$request->pid])->orderBy('created_at','DESC')->get()->toArray();

      $newsmobile = $news;

      $news = array_chunk($news,6);

      return view('show-notice', compact('post','news','newsmobile'));
    }


    public function sendProject(Request $request)
    {

      if ($request->file('project')->isValid()){


        $path = $request->file('project')->store('public/proyectos');

        $url = \Storage::url($path);


        $user =  auth()->user();

        $user->link_file = $url;
        $user->complete = true;
        $user->save();

        return redirect('/#upload')->with('success','Archivo enviado con éxito');

      }

        return redirect('/#error')->with('success','Archivo enviado con éxito');

    }

    public function privacy()
    {
        return view('privacy');
    }

    public function terms()
    {
        return view('terms');
    }

    public function cancel(Request $request)
    {
      $id = $request->id;

      $user =User::findOrFail($id);
      $user->cancelado = true;
      $user->save();

      if($user)
        return 'Tu suscripción ha sido cancelada.';

      return 'Error';
    }
}
