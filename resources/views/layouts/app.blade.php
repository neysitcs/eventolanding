<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

  <link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css?v=2.1') }}" rel="stylesheet">
</head>
<body>
    <div id="home">
        <nav  class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>


                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>


                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->

                              <li><a href="#homex">Inicio</a></li>

                        @guest
                            <li><a class="btn-login" href="javascript:;">Iniciar sesión</a></li>
                        @endguest

                            <li><a href="#noticias">Noticias</a></li>
                            <li><a href="#bases">Bases concurso</a></li>
                            <li><a href="#faq">Preguntas Frecuentes</a></li>
                            <li><a href="#contacto">Contacto</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')


        <div class="footer">
            <p>SIGUE EL CONCURSO EN NUESTRO FAN PAGE</p>

            <div class="center">
            <a href="https://www.facebook.com/Concurso-Cultura-por-un-Futuro-sin-Drogas-Edici%C3%B3n-2018-483825981984603/" target="_blank"><img src="images/fb.png"/></a>
            </div>

          <div class="footer-links">

            @guest
                <li><a class="btn-login" href="javascript:;">Iniciar sesión</a></li>
            @endguest
            <li><a href="#noticias">Noticias</a></li>
            <li><a href="#bases">Bases concurso</a></li>
            <li><a href="#faq">Preguntas Frecuentes</a></li>
            <li><a href="#contacto">Contacto</a></li>

          </div>


            <p class="terms"><a href="/terms">TÉRMINOS Y CONDICIONES </a>/ <a href="/privacy">POLÍTICAS DE PRIVACIDAD </a> / <a href="javascript:;">© DERECHOS RESERVADOS </a></p>

            <img class="logof" src="{{asset('/images/logo-fundacion.png')}}"/>

        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
</body>
</html>
