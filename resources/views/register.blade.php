<div class="section section-register group" id="register">

  <div class="bar-register">Regístrate</div>

  <div class="container">

    <div class="form-register col-md-8 col-md-offset-2">


<form class="form-horizontal" method="POST" action="{{route('register.user')}}">
  {!! csrf_field() !!}

  <br/>


  <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="col-sm-3 control-label">*Nombre</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" name="name" id="name"  value="{{ old('name') }}" placeholder="Nombres">
      @if ($errors->has('name'))
          <span class="help-block">
              <strong>{{ $errors->first('name') }}</strong>
          </span>
      @endif
    </div>
  </div>

  <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
    <label for="last_name" class="col-sm-3 control-label">Apellidos</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" name="last_name" id="last_name" value="{{ old('last_name') }}" placeholder="Apellidos">
      @if ($errors->has('last_name'))
          <span class="help-block">
              <strong>{{ $errors->first('last_name') }}</strong>
          </span>
      @endif
    </div>
  </div>

<div class="form-group{{ $errors->has('email_register') ? ' has-error' : '' }}">
<label for="email_register" class="col-sm-3 control-label">*Correo</label>
<div class="col-sm-5">
<input type="email" class="form-control" name="email_register" id="email_register" value="{{ old('email_register') }}" placeholder="Email">
@if ($errors->has('email_register'))
    <span class="help-block">
        <strong>{{ $errors->first('email_register') }}</strong>
    </span>
@endif
</div>

</div>
<div class="form-group{{ $errors->has('password_register') ? ' has-error' : '' }}">
<label for="password_register" class="col-sm-3 control-label">*Contraseña</label>
<div class="col-xs-10 col-sm-4">
<input type="password" class="form-control" name="password_register" id="password_register" value="{{ old('password_register') }}" placeholder="Contraseña">

<span class="infot" data-to="tpassword"></span>
<span class="toolx" id="tpassword">Válido solo uso de letras y/o números.</span>

@if ($errors->has('password_register'))
    <span class="help-block">
        <strong>{{ $errors->first('password_register') }}</strong>
    </span>
@endif
</div>
</div>

<div class="form-group">
<label for="dni" class="col-sm-3 control-label">DNI</label>
<div class="col-sm-4">
<input type="text" class="form-control" name="dni" id="dni" value="{{ old('dni') }}" placeholder="DNI">
@if ($errors->has('dni'))
    <span class="help-block" style="color:#a94442">
        <strong>{{ $errors->first('dni') }}</strong>
    </span>
@endif
</div>
<label for="city" class="col-sm-2 control-label">Ciudad</label>
<div class="col-sm-3">
{!! Form::select('city', $regions, old('city'),['class'=>'form-control']); !!}
</div>
</div>


<div class="form-group">
<label for="dni" class="col-xs-12 col-sm-3 control-label">Teléfono o celular</label>
<div class="col-xs-4 col-sm-2">
<input type="text" class="form-control" value="{{ old('prefix') }}" name="prefix" id="prefix" placeholder="Prefijo">
@if ($errors->has('prefix'))
    <span class="help-block" style="color:#a94442">
        <strong>{{ $errors->first('prefix') }}</strong>
    </span>
@endif
</div>
<div class="col-xs-6 col-sm-4">
  <input type="text" class="form-control" value="{{ old('phone') }}" name="phone" id="phone" placeholder="Número">

  <span class="infot" data-to="tprefix"></span>
  <span class="toolx" id="tprefix">Incluir código de región.</span>

  @if ($errors->has('phone'))
      <span class="help-block" style="color:#a94442">
          <strong>{{ $errors->first('phone') }}</strong>
      </span>
  @endif
</div>
</div>

<div class="form-group{{ $errors->has('university') ? ' has-error' : '' }}">
<label for="university" class="col-sm-3 control-label">Universidad</label>
<div class="col-sm-6">
  {!! Form::select('university',$universities, old('university'),['class'=>'form-control']); !!}
</div>
</div>
<div class="form-group{{ $errors->has('facultad') ? ' has-error' : '' }}">
<label for="facultad" class="col-sm-3 control-label">Facultad</label>
<div class="col-sm-4">
  {!! Form::text('facultad', old('facultad'),['class'=>'form-control']); !!}
</div>
</div>
<div class="form-group{{ $errors->has('grado_academico') ? ' has-error' : '' }}">
<label for="grado_academico" class="col-sm-3 control-label">Grado académico</label>
<div class="col-sm-4">
  {!! Form::select('grado_academico',$degrees,  old('grado_academico'),['class'=>'form-control']); !!}
</div>
</div>
<div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
<label for="grado_academico" class="col-sm-3 control-label">*Categoría a participar</label>

</div>

<div class="form-group">



<div class="hidden-xs col-sm-2">
</div>
<div class="col-xs-12 col-sm-3">

<div class="checkbox">
<label class="ck-custom">
  <input type="checkbox" name="category" value="educacion" @if (old('category')=='educacion') checked @endif>
    <span class="checkmark"></span>
</label>
  <span class="ck-info">Educación al NO consumo</span>

</div>


</div>

<div class="col-xs-12 col-sm-3">
<div class="checkbox">
<label class="ck-custom">
<input type="checkbox" name="category"  value="promocion" @if (old('category')=='promocion') checked @endif>
<span class="checkmark"></span>
</label>
<span class="ck-info">Promoción de cultivos alternativos</span>
</div>
</div>

<div class="col-xs-12 col-sm-3">
<div class="checkbox">
<label class="ck-custom">
<input type="checkbox"  name="category" value="ambos" @if (old('category')=='ambos') checked @endif>
<span class="checkmark"></span>
</label>
<span class="ck-info">Ambos</span>
</div>
</div>


</div>


@if ($errors->has('category'))
<br/>
<div class="form-group has-error">
<span class="help-block" style="text-align:center">
    <strong>{{ $errors->first('category') }}</strong>
</span>
  </div>
@endif

<br/>

<div class="form-group">
<div class="col-sm-12">
<div class="center">
<button type="submit" class="btn btn-danger">Registrarme</button>
</div>
</div>
</div>


<div class="center">
<p>Al presionar en REGISTRARME, usted acepta haber leído y aceptado las <a href="/privacy" target="_blank">Políticas de Privacidad</a> <br/>
(*) Campos obligatorios</p>
</div>

</form>

</div>

</div>

</div>
