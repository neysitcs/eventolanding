@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="{{asset('images/logomail.png')}}" style="width:120px;padding-top:5px;" />
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')

      @component('mail::subfooter')

      @endcomponent

        @component('mail::footer')
              <a href="{{$link}}">CANCELAR SUSCRIPCIÓN</a>
        @endcomponent
    @endslot
@endcomponent
