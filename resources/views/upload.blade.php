<div class="section section-upload group" id="upload">

  <div class="bar-register bar-edit" id="edit" @if ($errors->count()>0)
      style="display:block"
  @endif><div class="col-xs-12 col-sm-9 col-md-8 col-sm-ofsset-2 col-md-offset-2 col-custom">
    Editar Perfil <span>x</span></div>
  </div>

  <div class="container">



    @include('formedit')

    <div class="form-upload col-xs-12 col-sm-9 col-md-6 col-sm-ofsset-2 col-md-offset-3">
      <div class="center">
      <h4 class="hello">HOLA {{auth()->user()->name}}</h4>



      @if (  auth()->user()->complete ==true)

        <div class="alert alert-success">
          ¡Has enviado con éxito tu proyecto, gracias por participar!.
        </div>

      @else

      <p>Recuerda que tienes plazo hasta el 20 de Febrero para enviar tu proyecto.<br/>
Tú puedes ser el gran cambio que el Perú necesita. <b>¡EXITOS!</b></p>

<form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{route('user.project')}}">
  {!! csrf_field() !!}

  <br/>
  <div class="col-lg-12 col-sm-12 col-xs-12">
             <div class="input-group">
               <input type="text" class="form-control input-upload" readonly>
                 <label class="input-group-btn">
                     <span class="btn btn-primary btn-adjunt">
                         + Adjuntar<input type="file" name="project" style="display: none;">
                     </span>
                 </label>
             </div>

         </div>


<br/>

<div class="form-group">
  <div class="col-md-12">


    <button type="submit" class="btn btn-danger btn-block btn-subir">Enviar</button>

  </div>
</div>

<div class="line"></div>

@endif

<br/>

<a class="link-edit" href="#edit">Editar Perfil <img src="{{asset('images/edit.png')}}"/></a>

<br/>
<br/>

<a class="logout" href="{{url('logout')}}">Cerrar Sesión</a>


</form>

</div>

</div>

</div>
</div>
