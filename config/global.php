<?php

return  [
    'regions' =>  ['Seleccione','Amazonas','Ancash','Apurimac','Arequipa','Ayacucho','Cajamarca','Callao','Cusco','Huancavelica','Huanuco','Ica','Junin','La Libertad','Lambayeque','Lima','Loreto','Madre De Dios','Moquegua','Pasco','Piura','Puno','San Martin','Tacna','Tumbes','Ucayali',],

    'degrees'=>[
        'Seleccione',
        'Estudiante',
        'Bachiller',
        'Profesional',
        'Diplomado',
        'Licenciado',
     ],

     'contact'=>'mrios@rhadministraciones.com.pe',
];
