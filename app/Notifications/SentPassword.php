<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SentPassword extends Notification
{
    use Queueable;

    private $user;
    private $password;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {




        $message = (new MailMessage)
                    ->subject('Restablecer contraseña - futurosindrogas.com')
                    ->greeting('Hola '.$this->user->name.'!')
                    ->line('Te recordamos tus datos para que puedas ingresar a nuestro <a href="'.url('/').'">lading page</a> y sigas participando del concurso.')
                    ->action("Usuario: {$this->user->email} <br/>Contraseña: {$this->password}", url('/'))
                    ->line('<div class="lined">Por seguridad recuerda cambiar tu contraseña al iniciar sesión en la opción <b>"Editar Perfil"</b></div>');

        $message->salutation =url('/cancel?id='.$this->user->id);



        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
