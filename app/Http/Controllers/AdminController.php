<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;

use App\Post;
use App\User;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {

      return view('admin-news');

    }

    public function users()
    {

      $users = User::where('role','user')->get();

      return view('admin-users', compact('users'));
    }


    public function uploadFile(Request $request)
    {


      $path = $request->file('image')->store('public');

      $url = \Storage::url($path);

      return $url;

    }


    public function newsCreate(PostRequest $request)
    {
        $post = Post::create($request->all());


        return redirect()->back()->with('success','Post guardado con éxito');
    }

    public function newsIndex()
    {
        $posts = Post::all();

        return view('news-index',compact('posts'));
    }
    public function deletenew($id)
    {

      if(auth()->user()->role !='admin')
          die('No tienes Permiso para esta accion');

      $new = Post::findOrFail($id);

      $new->delete();

      return redirect()->back();

    }
    public function edit($id)
    {

      if(auth()->user()->role !='admin')
          die('No tienes Permiso para esta accion');

      $post = Post::findOrFail($id);


      return view('edit-new',compact('post'));


    }
    public function update(PostRequest $request, $id)
    {

      $post = Post::find($id);



      $post->update($request->all());

        return redirect()->to('/news');

    }
}
