@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Crear noticia</div>

                <div class="panel-body">

                    {!! Form::open(['method' => 'POST', 'route' => 'news.create', 'class' => 'form-horizontal']) !!}

                      <div class="col-sm-12">

                      <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <input type="hidden" name="image" id="image"/>
                          {!! Form::label('title', 'Titulo') !!}
                          {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
                          <small class="text-danger">{{ $errors->first('title') }}</small>
                      </div>

                      </div>

                    <div class="col-sm-12">
                        <div class="form-group{{ $errors->has('post') ? ' has-error' : '' }}">
                            {!! Form::label('post', 'Contenido') !!}
                            {!! Form::textarea('post', null, ['id'=>'summernote','rows'=>6, 'class' => 'form-control', 'required' => 'required']) !!}
                            <small class="text-danger">{{ $errors->first('post') }}</small>
                        </div>
                    </div>
                      <div class="col-sm-12">
                    <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                        {!! Form::label('Fecha', 'Date') !!}
                        {!! Form::date('date', old('date',date('Y-m-d')), ['class' => 'form-control', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('date') }}</small>
                    </div>
                      <div class="col-sm-12">

                      <div class="col-sm-12">
                        <div class="btn-group pull-right">


                            {!! Form::submit("Publicar", ['class' => 'btn btn-success']) !!}
                        </div>
                        </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
