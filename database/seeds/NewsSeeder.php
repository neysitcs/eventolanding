<?php

use Illuminate\Database\Seeder;

use App\Post;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $faker = Faker\Factory::create();


      for ($i=0; $i < 10; $i++) {
          Post::create([
            'title' => $faker->text(20),
            'post' => $faker->text(2000),
            'image' => $faker->imageUrl,
            'date' => $faker->date,
          ]);
      }

    }
}
