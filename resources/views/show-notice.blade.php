@extends('layouts.news')

@section('title',$post->title)

@section('content')

  <div class="container container-page">

    <h1 class="title">{{$post->title}}</h1>

    <div class="post">

      {!! $post->post !!}

      <div class="date-line">
            {!! (new DateTime($post->date))->format('d/m/Y') !!}
      </div>

      <div class="socials">
          <div class="share"><img src="images/share.png"/></div>
          <a  target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(route('news.show',['pid'=>$post->id]))}}">
            <span class="icon fb"></span></a>
            <a  target="_blank" href="https://plus.google.com/share?url={{urlencode(route('news.show',['pid'=>$post->id]))}}"><span class="icon google"></span></a>
     <a  target="_blank" href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&url={{urlencode(route('news.show',['pid'=>$post->id]))}}"><span class="icon twitter"></span></a>

            <a target="_blank"
            href="whatsapp://send?text={{route('news.show',['pid'=>$post->id]) }}" data-action="share/whatsapp/share"
            ><span class="icon whatsapp"></span></a>
      </div>

      <div class="comm">

        <div class="fb-comments" data-href="{{url('/noticias?pid='.$post->id)}}" data-width="100%" data-numposts="5"></div>

      </div>

    </div>

    <br/>
    <br/>
    <br/>

  </div>

  <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.11&appId=157035844647960';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<div class="section section-noticias group" id="noticias">
  <h2 class="title">Noticias</h2>


  <div id="carromobile" class="carousel slide visible-xs" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#carromobile" data-slide-to="0" class="active"></li>
      <li data-target="#carromobile" data-slide-to="1"></li>
      <li data-target="#carromobile" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
 <div class="carousel-inner">
     @foreach ($newsmobile as $key => $value)
   <div class="item @if($key==0) active @endif">
      <div class="col-sm-4">
        <div class="new">
            <img src="{{asset($value['image'])}}">
            <div class="new_title">{{$value['title']}}</div>
            <div class="new-wrapper">
            <div class="new_content">{{ substr(strip_tags($value['post']),0,100) }}</div>
          <div class="new_date">{{ (new DateTime($value['date']))->format('d/m/Y') }}</div>
            <br>
             <a class="readmore" href="/noticias?pid={{$value['id']}}">LEER MÁS</a>
              <br><br>
        </div>
        </div>
        </div>
   </div>

     @endforeach

  </div>
 </div>

  <div id="myCarousel" class="carousel slide hidden-xs" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
 <div class="carousel-inner">
   <div class="item active">

    @foreach ($news[0] as $key => $value)
      <div class="col-sm-4">
        <div class="new">
            <img src="{{asset($value['image'])}}">
            <div class="new_title">{{$value['title']}}</div>
            <div class="new-wrapper">
            <div class="new_content">{{ substr(strip_tags($value['post']),0,100) }}</div>
          <div class="new_date">{{ (new DateTime($value['date']))->format('d/m/Y') }}</div>
            <br>
             <a class="readmore" href="/noticias?pid={{$value['id']}}">LEER MÁS</a>
              <br><br>
        </div>
        </div>
        </div>
    @endforeach


   </div>

   <div class="item">

     @if (isset($news[1]))

     @foreach ($news[1] as $key => $value)
       <div class="col-sm-4">
         <div class="new">
             <img src="{{asset($value['image'])}}">
             <div class="new_title">{{$value['title']}}</div>
               <div class="new-wrapper">
             <div class="new_content">{{ substr(strip_tags($value['post']),0,100) }}</div>
            <div class="new_date">{{ (new DateTime($value['date']))->format('d/m/Y') }}</div>
            <br>
             <a class="readmore" href="/noticias?pid={{$value['id']}}">LEER MÁS</a>
              <br><br>
         </div>
         </div>
         </div>
     @endforeach

     @endif

   </div>

   <div class="item">
    @if (isset($news[2]))
     @foreach ($news[2] as $key => $value)
       <div class="col-sm-4">
         <div class="new">
             <img src="{{asset($value['image'])}}">
             <div class="new_title">{{$value['title']}}</div>
               <div class="new-wrapper">
             <div class="new_content">{{ substr(strip_tags($value['post']),0,100) }}</div>
             <div class="new_date">{{ (new DateTime($value['date']))->format('d/m/Y') }}</div>

             <br>
              <a class="readmore" href="/noticias?pid={{$value['id']}}">LEER MÁS</a>
               <br><br>

         </div>
         </div>
         </div>
     @endforeach
   @endif
   </div>
 </div>



</div>
</div>


<br><br>
<br>

@endsection
