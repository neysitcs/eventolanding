<?php

use Illuminate\Database\Seeder;

class UniversitiesTableSeeder extends Seeder
{

    protected function loadDB($file, $name)
    {

        if (Schema::hasTable($name) == true and \DB::table($name)->count() > 1) {
            $this->command->warn('OK. Las ' . ucfirst($name) . ' ya estan cargadas');
        } else {
            $this->command->info('Cargando DB, puede tardar unos minutos...');
            DB::unprepared(file_get_contents($file));
            $this->command->info(ucfirst($name) . ' cargado con éxito');
        }
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      //Universidades y paises
      $path = resource_path('db/');
      $unis = $path.'/universities.sql';
      $this->loadDB($unis, 'universities');

    }
}
