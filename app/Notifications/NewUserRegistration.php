<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewUserRegistration extends Notification
{
    use Queueable;

    private $user;
    private $password;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
     public function __construct($user, $password)
     {
         $this->user = $user;
         $this->password = $password;
     }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message =  (new MailMessage)
                    ->subject('Gracias por registrarte en futurosindrogas.com')
                    ->greeting('Hola '.$this->user->name.'!')
                    ->line('Tu registro ha sido <b>exitoso</b>.<br>Desde hoy podrás participar de nuestro concurso a nivel nacional.')
                    ->line('Para poder iniciar sesión en nuestro <a href="'.url('/').'">lading page</a> y poder subir tu proyecto necesitarás:')
                    ->action("Usuario: {$this->user->email} <br/>Contraseña: {$this->password}", url('/'))
                    ->line('<div class="linex"></div>');

          $message->salutation =url('/cancel?id='.$this->user->id);


        return $message;

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
